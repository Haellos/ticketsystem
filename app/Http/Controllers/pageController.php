<?php

namespace App\Http\Controllers;

class pageController extends Controller
{
    public function index()
    {
        return view('master/login');
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function class()
    {
        return view('class');
    }

    public function account()
    {
        return view('account');
    }
    
}
