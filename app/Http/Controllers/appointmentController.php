<?php

namespace App\Http\Controllers;

class appointmentController extends Controller
{
    public function index()
    {
        return view('appointments/index');
    }

    public function create()
    {
        return view('appointments/create');
    }
    
    public function store()
    {
        return view('appointments/store');
    }

    public function show()
    {
        return view('appointments/show');
    }

    public function edit() //Docent only
    {
        return view('appointments/edit');
    }
    
    public function destroy() //Docent only
    {
        return view('appointments/destroy');
    }
}