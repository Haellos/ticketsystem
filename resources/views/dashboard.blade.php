@extends('master/master')

@section('title')

    Dashboard

@endsection

@section('content')

<div id="content" class="col-md-10 col-12">

    {{-- Appointments --}}
    <div class="container-fluid shadow-border mt-3">
        <div class="row">

            <div class="col-12 rounded-top border-bottom p-0">
                <p class="h5 pl-3 pt-2">
                    {{-- Title --}}
                    Afspraken deze week:
                </p>
            </div>
            
            <div class="col-12 rounded-bottom py-2 mb-4">
                <div class="container-fluid text-center">
                    <p class="h4">
                        {{-- Date --}}
                        24-01-2019
                    </p>
                </div>
                {{-- Table with data --}}
                <div class="container-fluid border shadow-border border-dark">
                    {{-- Header for appointments --}}
                    <div class="row justify-content-center text-white bg-purple-alt text-center">
                        <div class="col-3">
                            <b><p class="mb-1"> Tijd </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Met wie </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Waar </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Onderwerp </p></b>
                        </div>
                    </div>
                    {{-- Appointment --}}
                    <div class="row justify-content-center text-center">
                        <div class="col-3 py-2 border-top border-dark">
                            13:20 - 14:00
                        </div>
                        <div class="col-3 py-2 border-top border-dark">
                            Jan Zuur
                        </div>
                        <div class="col-3 py-2 border-top border-dark">
                            Z3.02
                        </div>
                        <div class="col-3 py-2 border-top border-dark">
                            Project Ticketsysteem
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>    

    {{-- Recent chats (WIP) --}}
    <div class="container-fluid shadow-border mt-3">
        <div class="row">
            <div class="col-12 rounded-top border-bottom p-0">
                <p class="h5 pl-3 pt-2">Berichten :</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="border shadow-border border-dark ">
                    <h4> Thijs:</h4>
                    <p>Hallo wereld, hoi!</p>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection