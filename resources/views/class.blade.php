@extends('master/master')

@section('title')

    {{-- Echo title here  --}}
    Ao3
    
@endsection

@section('content')

<div id="content" class="col-12 col-md-10 p-0 screensized">
    <div class="container-fluid mt-0">
                            
        <!-- Titel -->
        <div class="row border-bottom pt-2 pb-2 chat-title">
            <div class="col-12">
                <p class="h2 mt-2 ml-2">
                    {{-- Echo title here --}}
                    @yield('title')
                </p>
            </div>
        </div>

        <!-- Chat -->
        <div class="row justify-content-center scroll chat-box">
            <div class="col-md-9 col-11">

                <!-- ChatMessage -->
                <div class="container border shadow-border bg-light mt-2 mb-2">
                    <div class="row">
                        <div class="col-12 border-bottom pt-1 pb-1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-4 p-0">
                                        <p class="m-0 ">
                                            <b>
                                                Henk Diever
                                                {{-- Username --}}
                                            </b>
                                        </p>
                                    </div>
                                    <div class="col-8 p-0">
                                        <p class="mb-0 text-right">
                                            (16:20 - 20/04/2019)
                                            {{-- Time and date : (16:20 - 20/04/2019) --}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-1 pb-1">
                            <p class="m-0">
                                {{-- Message --}}
                                Chatbericht
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <!-- Chatbalk -->
        <div class="row footer pb-2 pt-2 border-top">
            <div class="col-12">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-10 bg-white border text-muted">
                            Chat hier...
                            {{-- Input here --}}
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-10 text-right">
                            <i class="fas fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection