@extends('master/master')

@section('title')
    
    {{-- Echo title here  --}}
    Afspraken

@endsection

@section('content')
    
<div id="content" class="col-md-10 col-12">

    {{-- Button for nieuw appointment --}}
    <div class="container-fluid pt-2">
        <div class="row justify-content-md-end justify-content-center">
            <div class="col-6 text-center text-md-right">
                <a href="{{route('appointments.create')}}">
                    <button class="btn bg-purple-alt text-white">Nieuwe afspraak</button>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-3">
        <div class="row">

            {{-- Daily block --}}
            <div class="col-12 rounded-bottom pt-2 pb-2 mb-4">
                <div class="container-fluid">
                    {{-- Date --}}
                    <div class="row justify-content-center">
                        <p class="h4">24-01-2019</p>
                    </div>
                </div>
                <div class="container-fluid border shadow-border border-dark">
                    {{-- Header for appointments --}}
                    <div class="row justify-content-center text-white bg-purple-alt text-center">
                        <div class="col-3">
                            <b><p class="mb-1"> Tijd </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Met wie </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Waar </p></b>
                        </div>
                        <div class="col-3">
                            <b><p class="mb-1"> Onderwerp </p></b>
                        </div>
                    </div>
                    {{-- Appointment --}}
                    <div class="row justify-content-center text-center">
                        <div class="col-3 pt-2 pb-2 border-top border-dark">
                            13:20 - 14:00
                        </div>
                        <div class="col-3 pt-2 pb-2 border-top border-dark">
                            Jan Zuur
                        </div>
                        <div class="col-3 pt-2 pb-2 border-top border-dark">
                            Z3.02
                        </div>
                        <div class="col-3 pt-2 pb-2 border-top border-dark">
                            Project Ticketsysteem
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection

