@extends('master/master')

@section('title')

    New appointment

@endsection

@section('content')

    <div id="content" class="col-md-10 col-12">
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <form action="POST" class="justify-content-center">
                        @csrf

                        
                        
                        <input type="submit" value="Aanvragen" class="btn bg-purple-alt text-white">

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection