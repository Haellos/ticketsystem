<nav class="navbar navbar-expand-md navbar-dark bg-purple navbar-top">
    <a class="navbar-brand" href="#">@yield('title')</a>

    <button class="btn bg-purple-alt navbar-toggler" data-toggle="collapse" onclick="toggleSideNav()">
        <i class="fas fa-align-justify"></i>
    </button>
</nav>