<!DOCTYPE html>
<html class="bg-purple">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
    <body class="bg-purple">
        <div class="container" id="login-form-container">
            <div class="row justify-content-center">
                <div class="col-9 col-lg-6 text-center bg-light form-control p-5" id="login-form-col">
                    <form method="post">
                        <p class="h2 mb-5">Inloggen</p>
                        <div class="form-group">
                            <label class="mb-0" for="login_username">Gebruikersnaam</label>
                            <input class="form-control" type="text" name="login_username" id="login_username">
                        </div>
                        <div class="form-group mb-5">
                            <label class="mb-0" for="login_password">Wachtwoord</label>
                            <input class="form-control" type="text" name="login_password" id="login_password">
                        </div>                        
                    </form>
                    <a href="{{route('dashboard')}}">
                        <button class="btn bg-purple text-white">Log in!</button>
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>