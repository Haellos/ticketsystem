<nav id="side-navigation" class="col-md-2 d-none d-md-block bg-purple-alt screensized text-white side-nav">
    <div class="sidebar-sticky mt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{route('account')}}">
                    <p class="h6 {{ setActive('account') }}">
                        <i class="fas d-inline-block nav-icon fa-user"></i>
                        Account
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('dashboard')}}">
                    <p class="h6 {{ setActive('dashboard') }}">
                        <i class="fas d-inline-block nav-icon fa-columns"></i>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('appointments.index')}}">
                    <p class="h6 {{ setActive('appointments') }}">
                        <i class="fas d-inline-block nav-icon fa-calendar-check"></i>
                        Afspraken
                    </p>
                </a>
            </li> 
        </ul>
        
        <hr class="bg-white">

        {{-- <a class="nav-link" href="{{route('class')}}">
            <p class="h5 pl-2 {{ setActive('class') }}">
                <u>Klassenoverzicht</u>
            </p>
        </a> --}}
        
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{route('class')}}">
                    <p class="h6 {{ setActive('class') }}">
                        <i class="fas d-inline-block nav-icon fa-users"></i>
                        AO Dev 3
                    </p>
                </a>
            </li>
        </ul>
    </div>
</nav>