@extends('master/master')

@section('title')

    Account

@endsection

@section('content')
    
    <div id="content" class="col-md-10 col-12">
        <div class="container mt-5 bg-light shadow-border">
            <div class="row justify-content-center">
                <div class="col-6 p-4">
                    <img src="{{ URL::asset('img/penguin.png') }}" alt="logo" class="img-fluid img-icon m-auto d-block">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-12 pl-5">

                    <p class="m-0"><b>Voornaam:</b></p>
                    <p>Thijs</p>
                    
                    <p class="m-0"><b>Tussenvoegsel:</b></p>
                    <p>van</p>
                    
                    <p class="m-0"><b>Achternaam:</b></p>
                    <p>Duijn</p>

                </div>
                <div class="col-sm-6 col-12 pl-5">

                    <p class="m-0"><b>ID:</b></p>
                    <p>222285</p>
                    
                    <p class="m-0"><b>Mail:</b></p>
                    <p>Th.v.duyn@gmail.com</p>

                    <p class="m-0"><b>Beroep:</b></p>
                    <p>Student</p>

                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="text-center mt-3">
                        <u>
                            Log out
                        </u>
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection