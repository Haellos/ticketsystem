<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'pageController@index');

Route::get('/dashboard', 'pageController@dashboard')->name('dashboard');

Route::get('/class', 'pageController@class')->name('class');

Route::get('/account', 'pageController@account')->name('account');

/**
 * Appointments controller
 */
Route::get('/appointments', 'appointmentController@index')->name('appointments.index');

Route::get('/appointments/create', 'appointmentController@create')->name('appointments.create');


// Route::get('/appointments', 'appointmentController@index')->name('appointments.index');

// Route::get('/dashboard', 'dashboardController@index')->name('dashboard');

// Klassen Controller, tevens ook klassenoverzicht
// appointment controller
// De rest zijn losse pagina's


// Account settings
// Classes
// Class overview
// Appointments