var sideNav = { toggled:false }

toggleSideNav = function(){
    var nav = document.getElementById("side-navigation");
    var content = document.getElementById("content");
    if(sideNav.toggled === false){
      /*   nav.classList.remove("retract-side-nav"); */
        sideNav.toggled = true;
        nav.classList.add("toggle-side-nav");
        content.style.display = "none";
    } else{
        sideNav.toggled = false;
        nav.classList.remove("toggle-side-nav");
    /*     nav.classList.add("rectract-side-nav"); */
        content.style.display = "block";
    }
}